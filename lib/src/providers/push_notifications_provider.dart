

import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationProvider{

 FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

 final _mensajesStreamController = StreamController<String>.broadcast();
 Stream<String> get mensajes => _mensajesStreamController.stream;

  initNotifications(){
    _firebaseMessaging.requestNotificationPermissions();
    
    _firebaseMessaging.getToken().then((token){
      print('Token: ' + token );
      //e0pgTbybBl8:APA91bGh_pLwQful2-O5AXIom91vEpoa-EQu4gCb1RLmxi1cfLka_1-T0GjrcN2sFB8Ijfb-GDrmz8EYF12nF1G3XWFY15cIuuTJwkgYfW0GKQTg460oWF2EQbv3aC5rmKp1kAg7SaSJ
    });

    _firebaseMessaging.configure(

      onMessage: ( info ) async {
        print('====== ON MESSAGE ======');
        print(info);

        String argumento = 'no-data';
        if( Platform.isAndroid ){
          argumento = info['data']['foraneo'] ?? 'no-data';
        }
        _mensajesStreamController.sink.add(argumento);
      },
      onLaunch: ( info ) async{
        print('====== ON LAUNCH ======');
        print(info);
        
      },
      onResume: ( info ) async{
        print('====== ON RESUME ======');
        print(info);
        final noti = info['data']['foraneo'];
        print("EL FORANEO COCHINAZO ES: " + noti);
        
        String argumento = 'no-data';
        if( Platform.isAndroid ){
          argumento = info['data']['foraneo'] ?? 'no-data';
        }
        _mensajesStreamController.sink.add(argumento);
      }
    );  
  }

  dispose(){
    _mensajesStreamController?.close();
  }
}